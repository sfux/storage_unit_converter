# storage_unit_converter

This is a script to easily convert binary and decimal storage units

You can clone the script with:

~~~
git https://gitlab.ethz.ch/sfux/storage_unit_converter.git
~~~

It takes two parameters. The first parameter is a number and the second one a storage unit (kB,KiB,MB,MiB,GB,GiB,TB,TiB,PB,PiB). If the input is given in a binary unit (KiB,MiB,GiB,TiB,PiB), it is converted into a decimal unit and vice versa.

~~~
[sfux@eu-login-46 ~]$ ./storage_unit_converter.sh 12.45 gb
Input : 12.45 GB
Output: 11.59 GiB

[sfux@eu-login-46 ~]$ ./storage_unit_converter.sh 11.59 gib
Input : 11.59 GIB
Output: 12.44 GB

[sfux@eu-login-46 ~]$ ./storage_unit_converter.sh 40 Tb
Input : 40 TB
Output: 36.38 TiB

[sfux@eu-login-46 ~]$ ./storage_unit_converter.sh 36.38 tiB
Input : 36.38 TIB
Output: 40.00 TB

[sfux@eu-login-46 ~]$
~~~

The script does not work if the input is just bytes (then there is no reason to convert). It works for units from Kilo to Peta, but it can also be extended to higher powers if required.

The storage unit is not case sensitive. As long as you enter the correct letters it does not matter if they are lower/upper case or a mix.

You can specify interger values or floating point.
