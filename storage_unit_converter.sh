#!/bin/bash

AMOUNT=$1
SPACE_UNIT=${2^^}

# Function to convert Bytes into human readable format (decimal)
unitsd=( "B" "kB" "MB" "GB" "TB" "PB" "EB" "ZB" "YB" )
cnd(){
 exponentd=`echo "scale=3;floor(l($1)/l(1000))" | bc -l /cluster/apps/local/func.bc`
 numberd=`echo "($1)/(1000^$exponentd)" | bc -l`
 printf "Output: %0.2f %s" "$numberd" "${unitsd[$exponentd]}"
 echo -e "\n"
}

# Function to convert Bytes into human readable format (binary)
unitsb=( "B" "KiB" "MiB" "GiB" "TiB" "PiB" "EiB" "ZiB" "YiB" )
cnb(){
 exponentb=`echo "scale=3;floor(l($1)/l(1024))" | bc -l /cluster/apps/local/func.bc`
 numberb=`echo "($1)/(1024^$exponentb)" | bc -l`
 printf "Output: %0.2f %s" "$numberb" "${unitsb[$exponentb]}"
 echo -e "\n"
}

echo "Input : $AMOUNT $SPACE_UNIT"

case $SPACE_UNIT in
        KB)
                BAMOUNT=`echo "$AMOUNT*1000" | bc -l`
                cnb $BAMOUNT
        ;;
        KIB)
                BAMOUNT=`echo "$AMOUNT*1024" | bc -l`
                cnd $BAMOUNT
        ;;
        MB)
                BAMOUNT=`echo "$AMOUNT*1000*1000" | bc -l`
                cnb $BAMOUNT
        ;;
        MIB)
                BAMOUNT=`echo "$AMOUNT*1024*1024" | bc -l`
                cnd $BAMOUNT
        ;;
        GB)
                BAMOUNT=`echo "$AMOUNT*1000*1000*1000" | bc -l`
                cnb $BAMOUNT
        ;;
        GIB)
                BAMOUNT=`echo "$AMOUNT*1024*1024*1024" | bc -l`
                cnd $BAMOUNT
        ;;
        TB)
                BAMOUNT=`echo "$AMOUNT*1000*1000*1000*1000" | bc -l`
                cnb $BAMOUNT
        ;;
        TIB)
                BAMOUNT=`echo "$AMOUNT*1024*1024*1024*1024" | bc -l`
                cnd $BAMOUNT
        ;;
        PB)
                BAMOUNT=`echo "$AMOUNT*1000*1000*1000*1000*1000" | bc -l`
                cnb $BAMOUNT
        ;;
        PIB)
                BAMOUNT=`echo "$AMOUNT*1024*1024*1024*1024*1024" | bc -l`
                cnd $BAMOUNT
        ;;
esac
